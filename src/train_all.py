import torch
from typing import TypeVar
from tqdm import tqdm
import numpy as np
import gc
import torch.nn as nn

import argparse

from loader import load_dataset
from config import load_model


class Model(nn.Module):
    def __init__(self, model, config, nb_class=2, activation=nn.Sigmoid()):

        super(Model, self).__init__()
        self.img_size = config['img_size']
        self.feature_size = config['feature_size']
        self.model = model
        self.nb_class = nb_class
        self.head = nn.Linear(config['feature_size'], nb_class, bias=True)
        self.activation = activation

    def forward(self, x):
        out_feat = self.forward_feat(x)
        out = self.head(out_feat)
        out = self.activation(out)
        torch.cuda.device_of(x)
        return out

    def forward_feat(self, x):
        return self.model.forward(x)


def train_one_epoch(model, train_loader, criterion, optimizer, device):
    # keep track of training loss
    epoch_loss = 0.0
    epoch_accuracy = 0.0
    ###################
    # train the model #
    ###################
    model.train()

    for i, (data, target) in tqdm(enumerate(train_loader),
                                  total=len(train_loader)):
        # move tensors to GPU if CUDA is available
        data, target = data.to(device), target.to(device)

        # clear the gradients of all optimized variables
        optimizer.zero_grad()

        # forward pass: compute predicted outputs by passing inputs to the model
        output = model.forward(data)
        loss = criterion(output, target.argmax(dim=1))
        loss.backward()

        # Calculate Accuracy
        accuracy = (output.argmax(dim=1) == target.argmax(
            dim=1)).float().mean()

        # update training loss and accuracy
        epoch_loss += loss.item()
        epoch_accuracy += accuracy.item()

        # perform a single optimization step (parameter update)
        optimizer.step()

    return epoch_loss / len(train_loader), epoch_accuracy / len(train_loader)


def validate_one_epoch(model, valid_loader, criterion, device):
    # keep track of training loss
    valid_loss = 0.0
    valid_accuracy = 0.0
    valid_accuracy_bin = 0.0
    ######################
    # prevalid the model #
    ######################
    model.eval()

    for i, (data, target) in enumerate(valid_loader):
        # move tensors to GPU if CUDA is available
        data, target = data.to(device), target.to(device)

        with torch.no_grad():
            # forward pass: compute predicted outputs by passing inputs to the model

            output = model.forward(data)
            loss = criterion(output, target.argmax(dim=1))
            # Calculate Accuracy
            accuracy_bin = (
                ((output.argmax(dim=1) != 2).float() *
                 (target.argmax(dim=1) != 2).float()) * 0.5 +
                ((output.argmax(dim=1) == 2).float() *
                 (target.argmax(dim=1) == 2).float()) * 2).float().mean()
            accuracy = (output.argmax(dim=1) == target.argmax(
                dim=1)).float().mean()

            # update training loss and accuracy
            valid_loss += loss
            valid_accuracy += accuracy
            valid_accuracy_bin += accuracy_bin

    return valid_loss / len(valid_loader), valid_accuracy / len(
        valid_loader), valid_accuracy_bin / len(valid_loader)


def one_epoch(epoch, model, optimizer, criterion, device, train_loader,
              valid_loader):
    gc.collect()

    print("=" * 20)
    print(f"EPOCH {epoch} TRAINING...")

    train_loss, train_acc = train_one_epoch(model, train_loader, criterion,
                                            optimizer, device)
    train_acc = np.mean(train_acc)
    print(
        f"[TRAIN] EPOCH {epoch} - LOSS: {train_loss:2.4f}, ACCURACY:{train_acc:2.4f} "
    )
    gc.collect()
    valid_loss, valid_acc = 0, 0
    if valid_loader is not None:
        gc.collect()
        print("EPOCH " + str(epoch) + " - VALIDATING...")
        valid_loss, valid_acc, valid_acc_bin = validate_one_epoch(
            model, valid_loader, criterion, device)
        valid_acc = torch.mean(valid_acc)
        print(
            f"[VALID] LOSS: {valid_loss:2.4f}, ACCURACY:{valid_acc:2.4f} BIN_ACCURACY:{valid_acc_bin:2.4f}"
        )

        gc.collect()
    return train_loss, train_acc, valid_loss, valid_acc


def fit(
    model: torch.nn.Module,
    model_name: str,
    epochs: int,
    criterion,
    optimizer,
    scheduler,
    device,
    train_loader,
    valid_loader,
) -> None:

    # keeping track of losses as it happen
    train_losses = []
    valid_losses = []
    train_accs = []
    valid_accs = []
    best_valid_acc = 0.

    # To froze the model

    for epoch in range(1, epochs + 1):
        train_loss, train_acc, valid_loss, valid_acc = one_epoch(
            epoch,
            model,
            optimizer,
            criterion,
            device,
            train_loader,
            valid_loader,
        )
        print(f'LR={scheduler.get_last_lr()}')
        if valid_acc > best_valid_acc:
            print('--- ACC has improved, saving model ---')
            torch.save(model.state_dict(),
                       '/src/models/trained/' + model_name + '_best.pth')
            best_valid_acc = valid_acc

        valid_losses.append(valid_loss)
        valid_accs.append(valid_acc)
        train_losses.append(train_loss)
        train_accs.append(train_acc)
        scheduler.step()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        default="xception",
        type=str,
        help="Which model to train",
    )
    parser.add_argument(
        "--lr",
        default=2e-5,
        type=float,
        help="learning rate",
    )
    parser.add_argument(
        "-b",
        "--batch",
        default=8,
        type=int,
        help="batch size",
    )
    parser.add_argument(
        "-e",
        "--epoch",
        default=10,
        type=int,
        help="number of epochs",
    )

    args = parser.parse_args()
    MODEL = args.model
    LR = args.lr
    EPOCH = args.epoch
    BATCH_SIZE = args.batch
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = load_model(MODEL)
    OPTIMIZER = torch.optim.AdamW(model.parameters(), lr=LR)
    CRITERION = nn.CrossEntropyLoss()  #nn.BCELoss()
    SCHEDULER = torch.optim.lr_scheduler.StepLR(OPTIMIZER, 3, gamma=0.2)
    print('MODEL:       ', MODEL)
    print('LR:          ', LR)
    print('BATCH_SIZE:  ', BATCH_SIZE)
    print('EPOCH:       ', EPOCH)
    print('OPTIMIZER:   ', OPTIMIZER)
    print('CRITERION:   ', CRITERION)
    print('SCHEDULER:   ', SCHEDULER)
    train_loader, valid_loader, nb_class = load_dataset(
        "/data", model.img_size[0], BATCH_SIZE)
    model.head = nn.Linear(model.feature_size, nb_class, bias=True)
    model.to(DEVICE)
    fit(
        model,
        MODEL,
        EPOCH,
        CRITERION,
        OPTIMIZER,
        SCHEDULER,
        DEVICE,
        train_loader,
        valid_loader,
    )
