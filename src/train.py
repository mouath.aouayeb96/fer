import torch
from typing import TypeVar
from tqdm import tqdm
import numpy as np
import gc
import torch.nn as nn
from torch.autograd import Variable
from models.T2TViT.models.t2t_vit import t2t_vit_14,t2t_vit_24,t2t_vit_t_14,t2t_vit_t_24
from models import T2TViT
import argparse
import glob
from loader import load_dataset
from config import load_model


class Model(nn.Module):
    def __init__(self, model, config, nb_class=7):

        super(Model, self).__init__()
        self.img_size = config['img_size']
        self.feature_size = config['feature_size']
        self.model = model
        self.nb_class = nb_class
        self.head = nn.Linear(config['feature_size'], nb_class, bias=True)

    def forward(self, x):
        out_feat = self.forward_feat(x)
        out = self.head(out_feat)
        torch.cuda.device_of(x)
        return out

    def forward_feat(self, x):
        return self.model.forward(x)

class squeezeExcitaion(nn.Module):
    def __init__(self, n_classes, in_features,reduction_ratio=2):
        super(squeezeExcitaion, self).__init__()
        
        #self.transformer_out = layer
        self.in_features = in_features
        self.reduction_ratio = reduction_ratio
        self.num_features_reduced = self.in_features // reduction_ratio
        
        self.fc1 = nn.Linear(self.in_features, self.num_features_reduced, bias=True)
        self.fc2 = nn.Linear(self.num_features_reduced, self.in_features, bias=True)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()
        #norm_layer = None or partial(nn.LayerNorm, eps=1e-6)
        #self.norms = norm_layer(self.in_features)

        self.cls = nn.Linear(self.in_features,n_classes, bias=True)
    def forward(self, squeeze_tensor):
        #squeeze and excitation
        fc_out_1 = self.relu(self.fc1(squeeze_tensor))
        fc_out_2 = self.sigmoid(self.fc2(fc_out_1))
        output_tensor = torch.mul(squeeze_tensor, fc_out_2)
        
        #classificatioin
        y = self.cls(output_tensor)
        
        return y
    
def mixup_data(x, y, alpha=1.0, device=None):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1

    batch_size = x.size()[0]
    index = torch.randperm(batch_size).to(device)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam


def mixup_criterion(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)
    
def train_one_epoch(model, train_loader, criterion, reg, optimizer, device):
    # keep track of training loss
    epoch_loss = 0.0
    epoch_accuracy = 0.0
    ###################
    # train the model #
    ###################
    model.train()

    for i, (data, target) in tqdm(enumerate(train_loader),
                                  total=len(train_loader)):
        # move tensors to GPU if CUDA is available
        data, target = data.to(device), target.to(device)

        # clear the gradients of all optimized variables
        optimizer.zero_grad()
        
        if reg:
            #regulazation mixup
            data, targets_a, targets_b, lam = mixup_data(data, target,1., device)
            data, targets_a, targets_b = map(Variable, (data,targets_a, targets_b))
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model.forward(data)
        if reg:
            loss = mixup_criterion(criterion, output, targets_a, targets_b, lam).to(device, dtype=torch.float32)
        else:
            loss = criterion(output,target)
        loss.backward()

        # Calculate Accuracy
        accuracy = (output.argmax(dim=1) == target).float().mean()

        # update training loss and accuracy
        epoch_loss += loss.item()
        epoch_accuracy += accuracy.item()

        # perform a single optimization step (parameter update)
        optimizer.step()

    return epoch_loss / len(train_loader), epoch_accuracy / len(train_loader)


def validate_one_epoch(model, valid_loader, criterion, device):
    # keep track of training loss
    valid_loss = 0.0
    valid_accuracy = 0.0
    ######################
    # prevalid the model #
    ######################
    model.eval()

    for i, (data, target) in enumerate(valid_loader):
        # move tensors to GPU if CUDA is available
        data, target = data.to(device), target.to(device)

        with torch.no_grad():
            # forward pass: compute predicted outputs by passing inputs to the model

            output = model.forward(data)
            loss = criterion(output, target)

            # Calculate Accuracy
            accuracy = (output.argmax(dim=1) == target).float().mean()

            # update training loss and accuracy
            valid_loss += loss
            valid_accuracy += accuracy

    return valid_loss / len(valid_loader), valid_accuracy / len(valid_loader)


def one_epoch(epoch, model, optimizer, criterion, reg, device, train_loader,
              valid_loader):
    gc.collect()

    print("=" * 20)
    #print(f"EPOCH {epoch} TRAINING...")

    train_loss, train_acc = train_one_epoch(model, train_loader, criterion, reg,
                                            optimizer, device)
    train_acc = np.mean(train_acc)
    #print(
    #    f"[TRAIN] EPOCH {epoch} - LOSS: {train_loss:2.4f}, ACCURACY:{train_acc:2.4f} "
    #)
    gc.collect()
    valid_loss, valid_acc = 0, 0
    if valid_loader is not None:
        gc.collect()
        #print(f"EPOCH {epoch} VALIDATING...")
        valid_loss, valid_acc = validate_one_epoch(model, valid_loader,
                                                   criterion, device)
        valid_acc = torch.mean(valid_acc)
        #print("[VALID] LOSS: {:2.4f}, ACCURACY:{:2.4f} ".format(
        #    valid_loss, valid_acc))

        gc.collect()
    return train_loss, train_acc, valid_loss, valid_acc


def fit(
    model: torch.nn.Module,
    model_name: str,
    epochs: int,
    criterion,
    optimizer,
    scheduler,
    reg,
    device,
    train_loader,
    valid_loader,
    name,
) -> None:

    # keeping track of losses as it happen
    train_losses = []
    valid_losses = []
    train_accs = []
    valid_accs = []
    best_valid_acc = 0.
    epc = 0
    # To froze the model

    for epoch in range(1, epochs + 1):
        train_loss, train_acc, valid_loss, valid_acc = one_epoch(
            epoch,
            model,
            optimizer,
            criterion,
            reg,
            device,
            train_loader,
            valid_loader,
        )
        if scheduler is not None:
            print(f'LR={scheduler.get_last_lr()}')
        if valid_acc > best_valid_acc:
            print('--- ACC has improved, saving model ---')
            torch.save(model.state_dict(),
                       './src/models/trained/'+name+'/' + model_name + '_best.pth')
            best_valid_acc = valid_acc
            epc = epoch
        valid_losses.append(valid_loss)
        valid_accs.append(valid_acc)
        train_losses.append(train_loss)
        train_accs.append(train_acc)
        if scheduler is not None:
            scheduler.step()
    print('\n\t the lat accuracy is : ',valid_acc) 
    print('\n\t the best accuracy is : ',best_valid_acc , '\tat epoch number : ', epc)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        default="vit_b_16",
        type=str,
        help="Which model to train",
    )
    parser.add_argument(
        "--lr",
        default=2e-5,
        type=float,
        help="learning rate",
    )
    parser.add_argument(
        "-b",
        "--batch",
        default=8,
        type=int,
        help="batch size",
    )
    parser.add_argument(
        "-e",
        "--epoch",
        default=10,
        type=int,
        help="number of epochs",
    )
    parser.add_argument(
        "-se",
        "--squeezeexcitation",
        default='False',
        type=str,
        help="Add a squeeze and excitation block on top of the model",
    )
    parser.add_argument(
        "--data",
        default='sfew',
        type=str,
        help="Path to the global content of all data",
    )
    parser.add_argument(
        "--scheduler",
        default='lr_scheduler',
        type=str,
        help="Scheduler",
    )
    parser.add_argument(
        "--dataAug",
        default='True',
        type=str,
        help="Train with Data Augmentation",
    )
    parser.add_argument(
        "-reg",
        "--Regulizer",
        default='True',
        type=str,
        help="Train with Cutout and Mixup regulizer",
    )
    
    parser.add_argument(
        "--use_fer13",
        default='True',
        type=str,
        help="use the pretrained model FER 2013",
    )
    
    
    args = parser.parse_args()
    MODEL = args.model
    SE = args.squeezeexcitation == 'True'
    FER13 = 'True' == args.use_fer13
    DATA = args.data
    LR = args.lr
    EPOCH = args.epoch
    BATCH_SIZE = args.batch
    SCHD = args.scheduler
    DA = args.dataAug == 'True'
    REG = args.Regulizer == 'True'
    
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = load_model(MODEL)
    if SE:
        model.head = squeezeExcitaion(n_classes=7,in_features=model.head.in_features,reduction_ratio=2)
    OPTIMIZER = torch.optim.AdamW(model.parameters(), lr=LR)
    CRITERION = nn.CrossEntropyLoss()
    if SCHD == 'lr_scheduler':
        SCHEDULER = torch.optim.lr_scheduler.StepLR(OPTIMIZER, 20, gamma=0.2)
    else:
        #print('No used Scheduler')
        SCHEDULER = None
        
    print('MODEL       : ', MODEL)
    print('SE attn     : ', SE)
    print('PRETRAIN FER: ', FER13)
    print('LR          : ', LR)
    print('BATCH_SIZE  : ', BATCH_SIZE)
    print('EPOCH       : ', EPOCH)
    print('CRITERION   : ', CRITERION)
    print('DATA AUG    : ', DA)
    print('REGULIZER   : ', REG)
    print('DATA TRAIN  : ', DATA)
    print('SCHEDULER   : ', SCHEDULER)
    print('OPTIMIZER   : ', OPTIMIZER)
    
    print('\n[LOAD] ...')
    train_loader, valid_loader, _ = load_dataset("./data",DATA, model.img_size[0],BATCH_SIZE,DA,REG)
    print('[DONE!]\n')
    if SE:
        MODEL += '_SE'
    if FER13:
        model.load_state_dict(torch.load('./src/models/trained/fer13/' + MODEL + '_best.pth'))
    model.to(DEVICE)
    
    fit(
        model,
        MODEL,
        EPOCH,
        CRITERION,
        OPTIMIZER,
        SCHEDULER,
        REG,
        DEVICE,
        train_loader,
        valid_loader,
        DATA,
    )
    
    print('\n[FINISH]')
