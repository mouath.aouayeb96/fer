import os
import gc

print('*'*200)
print('\n\t\t\t\t\t\t\t\t\tRun it all in once :)\n')
print('*'*200)

models = ['vit_s_16','vit_b_16','vit_res_b_16','tnt_s_16','t2t_14',
         't2t_24','t2t_t_14','t2t_t_24','swin_s_p4w7','swin_b_p4w7','swin_l_p4w7']

datas = ['fer13','sfew','rafdb']
#datas = ['rafdb','sfew']
for data in datas:
    print('\n')
    print('\t\t\t\t\t\t\t\t\tdata : ', data)
    print('/\\'*100)
    for model in models:  
        print('\n')
        print('\t\t\t\t\t\t\t\t\tmodel : ', model)
        # pretrained on FER13 : No ; SE block : No
        os.system('python src/train.py --epoch 50 --data '+data+' --scheduler nope -m '+model+' -se False --use_fer13 False')
        gc.collect()
        # pretrained on FER13 : No ; SE block : Yes
        os.system('python src/train.py --epoch 50 --data '+data+' --scheduler nope -m '+model+' -se True --use_fer13 False')
        gc.collect()
        if data != 'fer13':
            # pretrained on FER13 : Yes ; SE block : No
            os.system('python src/train.py --epoch 50 --data '+data+' --scheduler nope -m '+model+' -se False --use_fer13 True')
            gc.collect()
            # pretrained on FER13 : Yes ; SE block : Yes
            os.system('python src/train.py --epoch 50 --data '+data+' --scheduler nope -m '+model+' -se True --use_fer13 True')
            gc.collect()
        print('\n')
        print('#'*200)
    
    print('\n')
    print('/\\'*100)
