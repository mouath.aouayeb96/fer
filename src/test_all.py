import torch
import torch.nn as nn

import argparse

from loader import load_test
from config import load_model
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc, f1_score
import matplotlib.pyplot as plt


class Model(nn.Module):
    def __init__(self, model, config, nb_class=2, activation=nn.Sigmoid()):

        super(Model, self).__init__()
        self.img_size = config['img_size']
        self.feature_size = config['feature_size']
        self.model = model
        self.nb_class = nb_class
        self.head = nn.Linear(config['feature_size'], nb_class, bias=True)
        self.activation = activation

    def forward(self, x):
        out_feat = self.forward_feat(x)
        out = self.head(out_feat)
        out = self.activation(out)
        torch.cuda.device_of(x)
        return out

    def forward_feat(self, x):
        return self.model.forward(x)


def get_prediction(model, device, test_loader):
    outputs = None
    labels = None
    for i, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)

        with torch.no_grad():
            output = model.forward(data)
            if outputs == None:
                outputs = output
                labels = target
            else:
                outputs = torch.cat((outputs, output), 0)
                labels = torch.cat((labels, target), 0)

    print(outputs.size())
    return outputs.detach().cpu().numpy(), labels.detach().cpu().numpy()


def accuracy(outputs, labels):
    return (outputs.argmax(axis=1) == labels[:, 1]).astype(float).mean()


def separated_accuracy(outputs, labels):
    print(labels[:, 0])
    reels = (outputs[labels[:, 0] == 0].argmax(
        axis=1) == labels[labels[:, 0] == 0, 1]).astype(float).mean()
    fakes = (outputs[labels[:, 0] == 1].argmax(
        axis=1) == labels[labels[:, 0] == 1, 1]).astype(float).mean()
    print('fake', (labels[:, 0] == 0).sum())
    print('reels', (labels[:, 0] == 1).sum())
    return reels, fakes


def roc(y_test, y_score, n=1):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(2):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    plt.figure()
    lw = 2
    print(fpr)
    plt.plot(fpr[n],
             tpr[n],
             color='darkorange',
             lw=lw,
             label=f'ROC curve (area = {roc_auc[n]:0.5})')
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        default="t2t_14",
        type=str,
        help="Which model to train",
    )
    parser.add_argument(
        "-w",
        "--weights",
        required=True,
        type=str,
        help="path to weights",
    )
    parser.add_argument(
        "-b",
        "--batch",
        default=32,
        type=int,
        help="batch size",
    )

    args = parser.parse_args()
    MODEL = args.model
    BATCH_SIZE = args.batch
    WEIGHTS = args.weights
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = load_model(MODEL)
    model.load_state_dict(torch.load(WEIGHTS))
    model.to(DEVICE)
    model.eval()
    print('MODEL:       ', MODEL)
    print('WEIGHTS:     ', WEIGHTS)
    print('BATCH:       ', BATCH_SIZE)
    test_loader = load_test("/data", model.img_size[0], BATCH_SIZE)
    outputs, labels = get_prediction(model, DEVICE, test_loader)
    roc(labels, outputs)
    print(accuracy(outputs, labels))
    print(separated_accuracy(outputs, labels))
