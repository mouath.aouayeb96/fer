import torch
import torch.nn as nn

import argparse

from loader import load_test
from config import load_model
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc, f1_score
import matplotlib.pyplot as plt


class Model(nn.Module):
    def __init__(self, model, config, nb_class=7):

        super(Model, self).__init__()
        self.img_size = config['img_size']
        self.feature_size = config['feature_size']
        self.model = model
        self.nb_class = nb_class
        self.head = nn.Linear(config['feature_size'], nb_class, bias=True)

    def forward(self, x):
        out_feat = self.forward_feat(x)
        out = self.head(out_feat)
        torch.cuda.device_of(x)
        return out

    def forward_feat(self, x):
        return self.model.forward(x)


def get_prediction(model, device, test_loader):
    outputs = None
    labels = None
    model.eval()
    for i, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)

        with torch.no_grad():
            output = model.forward(data)
            if outputs == None:
                outputs = output
                labels = target
            else:
                outputs = torch.cat((outputs, output), 0)
                labels = torch.cat((labels, target), 0)

    print(outputs.size())
    return outputs.detach().cpu().numpy(), labels.detach().cpu().numpy()


def accuracy(outputs, labels):
    return (outputs.argmax(dim=1) == labels).float().mean()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        default="t2t_14",
        type=str,
        help="Which model to train",
    )
    parser.add_argument(
        "-w",
        "--weights",
        required=True,
        type=str,
        help="path to weights",
    )
    parser.add_argument(
        "-b",
        "--batch",
        default=32,
        type=int,
        help="batch size",
    )
    parser.add_argument(
        "--data",
        default='sfew',
        type=str,
        help="Path to the global content of all data",
    )

    args = parser.parse_args()
    MODEL = args.model
    BATCH_SIZE = args.batch
    WEIGHTS = args.weights
    DATA = args.data
    
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = load_model(MODEL)
    model.load_state_dict(torch.load(WEIGHTS))
    model.to(DEVICE)
    model.eval()
    print('MODEL:       ', MODEL)
    print('WEIGHTS:     ', WEIGHTS)
    print('BATCH:       ', BATCH_SIZE)
    test_loader, _ = load_test("./data",DATA, model.img_size[0],BATCH_SIZE)
    outputs, labels = get_prediction(model, DEVICE, test_loader)
    print(accuracy(outputs, labels))
